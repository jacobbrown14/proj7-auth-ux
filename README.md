# README #

Author: Jacob Brown, jbrown14@uoregon.edu
Project 7, 12/01/2018, for CIS 322, Fall 2018

## Description:

A Flask, MongoDB, RESTful API implementation of an ACP Brevet Control Times Calculator. Extended from the previous project to include user registration, login, and token-based authentication.

## Instructions:

(Docker) Use start.sh to build and run the webapp service, laptop service, and database service:

[Webapp Service]: Go to "localhost:5000" in a browser to see the submission form page. Enter the values, then click "Submit." Click on "Display" to see all of your entries calculated with the correct opening and closing times.

[Laptop Service]: Go to "localhost:5001" to find three options: "Login," "Register New User," "Reauthenticate," and "Logout." Try "Register a New User" first -- after creating a new user, you will be redirected back to the homepage, where you can login as this user. You can reauthenticate your session from this page as well. Finally, use "Logout" to do just that.

NOTES: 

- If you run into any difficulties attempting to use "start.sh" again, try using "rebuild.sh" instead. This will force a rebuild of every service in docker-compose.yml.
- Apologies - token authentication across other resources doesn't work.
