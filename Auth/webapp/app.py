"""
Takes form information from submit.html and
returns the correct page with appropriate data.

Edited by Jacob Brown, 11/20/2018
for Project 6, CIS 322, Fall 2018
"""
import os, arrow, acp_times
from datetime import datetime

from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tracks

TIME_FORMAT = "%Y-%m-%dT%H:%M:%S%z"

# tzBugFixer() - fixes an annoying bug involving an extra ":" in the arrow 
# string format. I swear this didn't exist in the last project. This idea is from:
# https://stackoverflow.com/questions/30999230/parsing-timezone-with-colon
def tzBugFixer(time):

    if ":" == time[-3:-2]:
        return time[:-3]+time[-2:]
    return time

# index() - returns the submission page
@app.route('/')
def index():
    
    return render_template('submit.html')

# submit() - once form information has been submitted on
# submit.html, we're routed to this function to manipulate
# and submit the form data into the db, and then we redirect
# back to index() for more submissions
@app.route('/submit', methods=['POST'])
def submit():

    # Get form stuff
    distance = request.form.get('distance', 999, type=float)
    control = request.form.get('control', 999, type=float)
    bdate = request.form.get('bdate', "1986-12-06", type=str)
    btime = request.form.get('btime', "19:36", type=str)

    date_str = bdate + " " + btime
    start_time = str(arrow.get(date_str))

    # Some quick error checking...
    if control == 999 or control == 0:
        return error("ERROR: No control distance was entered")
    elif control < 0:
        return error("ERROR: Control distance is a negative number")
    elif control >= 1300:
        return error("ERROR: Control distance is greater than brevet maximum distance")

    if acp_times.percentage_checker(control, distance) == 1:
        return error("WARNING: Control distance is 20% or longer than brevet distance")
  
    if (int)(bdate.split('-', 1)[0])  > 3000:
        return error("ERROR: Bicycles will no longer exist beyond the year 3000")

    # First, we calculate and get strings using acp_times.py
    start_open = acp_times.open_time(0, distance, start_time)
    start_close = acp_times.close_time(0, distance, start_time)
    control_open = acp_times.open_time(control, distance, start_time)
    control_close = acp_times.close_time(control, distance, start_time)
    
    # Fixes a timezone format bug
    start_open = tzBugFixer(start_open)
    start_close = tzBugFixer(start_close)
    control_open = tzBugFixer(control_open)
    control_close = tzBugFixer(control_close)    

    # Then we convert these to the isoformatting accepted by MongoDB
    start_open_iso = datetime.strptime(start_open, TIME_FORMAT)
    start_close_iso = datetime.strptime(start_close, TIME_FORMAT)
    control_open_iso = datetime.strptime(control_open, TIME_FORMAT)
    control_close_iso = datetime.strptime(control_close, TIME_FORMAT)

    item_doc = {
        'distance' : distance,
        'control' : control,

        'start_open' : start_open_iso,
        'start_close' : start_close_iso,
        'control_open' : control_open_iso,
        'control_close' : control_close_iso,
    }
   
    db.collection.insert(item_doc)

    return redirect(url_for('index'))

# display() - displays all the database information
# on the display.html page
@app.route('/display', methods=['POST'])
def display():
    
    _items = db.collection.find()
    items = [item for item in _items]

    msg = ""

    if len(items) == 0:
        msg = "WARNING: Empty database. Go back and try again."

    return render_template('display.html', items=items, msg=msg)

# Basic error handlers for form submission
def error(error):
    return render_template('error.html', error=error)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
