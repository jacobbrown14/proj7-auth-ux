"""
User class to help reduce size of api.py

By Jacob Brown, 12/01/2018
for Project 7, CIS 322, Fall 2018
"""
from flask_login import LoginManager, UserMixin

class User(UserMixin):
    def __init__(self, username):
        self.username = username
        self.active = True
        self.authenticated = True
        self.anonymous = False

    def is_active(self):
        return self.active

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return self.anonymous

    def get_id(self):
        return self.username

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
