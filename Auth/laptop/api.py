"""
REST API that uses login, registration, and tokens to expose 
what's in our database in either CSV or JSON format

Edited by Jacob Brown, 12/01/2018
for Project 7, CIS 322, Fall 2018

CSV formatting idea from:
https://stackoverflow.com/questions/9157314/how-do-i-write-data-into-csv-format-as-string-not-file
"""
import os, io, csv, functools, base64
from flask import Flask, jsonify, request, render_template, redirect, url_for, flash, session
from flask_restful import Resource, Api, request, reqparse
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, fresh_login_required, confirm_login
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token
from pymongo import MongoClient
from wtforms import Form, BooleanField, StringField, PasswordField
from wtforms.validators import DataRequired
from user import User

# Instantiate the app
app = Flask(__name__)
api = Api(app)

SECRET_KEY = "It's a secret to everybody"
DEBUG = True

app.config.from_object(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tracks
users = client.users  #using db to store users

login_manager = LoginManager()
login_manager.login_view = "login"
login_manager.login_message = "Please login to access this page"
login_manager.refresh_view = "reauth"

@login_manager.user_loader
def load_user(username):
    user = users.collection.find_one({"username" : username})
    if user:
        username = user["username"]
        return User(username)
    return None

login_manager.setup_app(app)

@app.route("/")
def index():
    return render_template("index.html")

# api_token_required() - Checks if an authorized token exists in this
# session. If so, returns appropriately. Idea and code from:
# https://stackoverflow.com/questions/32510290/how-do-you-implement-token-authentication-in-flask
def api_token_required(func):
    @functools.wraps(func)
    def check_token(*args, **kwargs):
        # NOTE - flask.session doesn't work with cURL
        if "token_auth" in session:
            token = session["token_auth"]
            if verify_auth_token(token):
                return func(*args, **kwargs)
        return {"message" : "Unable to detect session token"}
    return check_token

# valid_user() - helper function used to check if a user exists
# in database based on their username and password
def valid_user(username, password):

    # Check if the username or password are valid first
    if username != "" and password != "":

        # Then check if the user actually exists and is valid
        user = users.collection.find_one({"username" : username})
        if user:

            # Verifies password against hash stored in db
            if verify_password(password, user["password"]):
                return 1
    return 0

# user_exists() - helper function used to check merely if a
# username already exists in the database
def user_exists(username):
    if users.collection.find_one({"username" : username}):
        return 1
    return 0

# secret() - Handy function that returns the secret page if a
# user passes a wrapper's test. Used for debug purposes.
@app.route("/secret")
@fresh_login_required
def secret():
    return render_template("secret.html")

# login() - Allows users to enter their username/password and
# checks if it exists in the database. Redirects appropriately.
@app.route("/login", methods=["GET", "POST"])
def login():
    
    # Form validation stuff
    form = LoginForm(request.form)

    # Checks method and validates form entry
    if request.method == "POST" and form.validate():
        username = form.username.data
        password = form.password.data
 
        # Sees if user exists in database
        if valid_user(username, password) == 1:
                        
            # Create a user obj from username and try to log 'em in
            user_obj = User(username)
            if login_user(user_obj, remember=form.remember.data):
                flash("Logged in successfully!")
                return redirect(request.args.get("next_page") or url_for("index"))

        else:
            flash("User not found - try again")
            return render_template("login.html", form=form)

    return render_template("login.html", form=form)

@app.route("/reauth", methods=["GET", "POST"])
@login_required
def reauth():
    if request.method == "POST":
        confirm_login()
        flash("Reauthenticated!")
        return redirect(request.args.get("next") or url_for("index"))
    return render_template("reauth.html")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out!")
    return redirect(url_for("index"))

@app.route("/register", methods=['GET', 'POST'])
def reg_test():

    # Form validation stuff
    form = RegistrationForm(request.form)

    # Checks method and validates form entry
    if request.method == 'POST' and form.validate():
        username_form = form.username.data
        password_form = form.password.data

        # Makes sure user doesn't already exist in database
        if user_exists(username_form) == 0:

            # Hash password
            hashed = hash_password(password_form)
        
            # User stuff to be added to database
            user_doc = {
                'username' : username_form,
                'password' : hashed,
            }

            flash("New user successfully registered!")
            users.collection.insert(user_doc)

            return redirect(url_for("index"))
        else:
            flash("That username is already in use")

    return render_template("register.html", form=form)

class Register(Resource):
    def post(self):
         
        # Argument parsing stuff from
        # https://flask-restful.readthedocs.io/en/0.3.5/quickstart.html
        parser = reqparse.RequestParser()
        parser.add_argument("username")
        parser.add_argument("password")
        args = parser.parse_args(strict=True)

        username = args["username"]
        password = args["password"]
 
        # Makes sure username, password are valid
        if username != "" and password != "":
            
            # Makes sure user doesn't already exist in database
            if user_exists(username) == 0:

                # Hash password before storing it
                hashed = hash_password(password)

                # User stuff to be stored in db
                user_doc = {
                    "username" : username,
                    "password" : hashed,
                }

                # Inserts new user into db
                users.collection.insert(user_doc)

                # Grabs user back to get its id for location
                user = users.collection.find_one({"username" : username})
                id = str(user["_id"])

                # Gathers a so-called "location," username to be returned
                json_doc = {
                    "location" : "http://localhost:5001/api/user/{}".format(id),
                    "username" : username,
                }

                # Returns JSON stuff, created code
                return json_doc, 201

        bad_doc = {"message" : "Bad request"}
        return bad_doc, 400

# Token() - Used to generate a token via cURL with a username and
# password. Token is valid for 5 minutes.
class Token(Resource):
    def get(self):

        # Token-based authorization
        basic_auth = request.headers.get('Authorization')
        username = ""
        password = ""

        # Decode and split authorization stuff received         
        if basic_auth:
            auth_token = basic_auth.split(' ')[1]
            user_info = base64.b64decode(auth_token).decode()
            username, password = user_info.split(':')
        else:
            return {"message" : "Bad HTTP Basic Authorization"}

        # Checks if user is valid first
        if valid_user(username, password) == 1:

                # Sets duration (in seconds) and create token
                duration = 300
                token = generate_auth_token(duration)

                # Store token as session variable.
                session["token_auth"] = token

                # Assemble token and duration return
                json_doc = {
                    "token" : token.decode(),
                    "duration" : duration,
                }

                return json_doc, 201
        
        return {"message" : "Bad username or password"}, 401

# WTForm for registering a new user
class RegistrationForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])

# WTForm for logging in
class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember = BooleanField('remember') # no validator??

## *** Project 6 Functions and Classes Start Here ***

# getListAllJSON() - returns all entries in db
# in handy JSON format
def getListAllJSON():
    
    dict = []

    for i in db.collection.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
            'control_close' : i['control_close']
        })

    list_all = {'ListAll' : dict}
    return jsonify(list_all)

# getListAllCSV() - returns all entries in db
# in handy CSV format
def getListAllCSV():

    list_all = io.StringIO()
    dict = []

    for i in db.collection.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
            'control_close' : i['control_close']
        })

    writer = csv.writer(list_all, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    new_str = formatCSVString(str(list_all.getvalue()))

    return new_str

# getListOpenOnlyJSON() - returns open times
# of all entries in JSON format
def getListOpenOnlyJSON():

    dict = []

    for i in db.collection.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open']
        })

    list_open_only = {"ListOpenOnly" : dict}
    return jsonify(list_open_only)

# getListOpenOnlyCSV() - returns open times
# of all entries in CSV format
def getListOpenOnlyCSV():

    list_open = io.StringIO()
    dict = []

    for i in db.collection.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_open' : i['control_open'],
        })

    writer = csv.writer(list_open, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    new_str = formatCSVString(str(list_open.getvalue()))

    return new_str

# getListCloseOnlyJSON() - returns close times
# of all entries in JSON format
def getListCloseOnlyJSON():

    dict = []

    for i in db.collection.find():
        dict.append({            
            'control' : i['control'],
            'distance' : i['distance'],
            'control_close' : i['control_close']
        })

    list_close_only = {"ListCloseOnly" : dict}
    return jsonify(list_close_only)

# getListCloseOnlyCSV() - returns close times
# of all entries in CSV format
def getListCloseOnlyCSV():

    list_close = io.StringIO()
    dict = []

    for i in db.collection.find():
        dict.append({
            'control' : i['control'],
            'distance' : i['distance'],
            'control_close' : i['control_close'],
        })

    writer = csv.writer(list_close, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerow(dict)

    # Tweaking the formatting of the CSV string a bit...
    new_str = formatCSVString(str(list_close.getvalue()))

    return new_str

# getListOpenOnlyTopJSON(k) - returns 'k' number
# of sorted open times in JSON format
def getListOpenOnlyTopJSON(k):

    dict = []
    
    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in db.collection.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_open' : i['control_open']
            })

    list_open_top = {"ListOpenOnlyTop" : dict}
    return jsonify(list_open_top)

# getListOpenOnlyTopCSV(k) - returns 'k' number
# of sorted open times in CSV format
def getListOpenOnlyTopCSV(k):

    list_open = io.StringIO()
    new_str = ""

    dict = []

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in db.collection.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_open' : i['control_open'],
            })

        writer = csv.writer(list_open, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(dict)

        # Tweaking the formatting of the CSV string a bit...
        new_str = formatCSVString(str(list_open.getvalue()))

    return new_str

# getListCloseOnlyTopJSON(k) - returns 'k' number
# of sorted close times in JSON format
def getListCloseOnlyTopJSON(k):
    
    dict = []

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in db.collection.find().sort([('control_close', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_close' : i['control_close']
            })

    list_close_top = {"ListCloseOnlyTop" : dict}
    return jsonify(list_close_top)

# getListCloseOnlyTopCSV(k) - returns 'k' number
# of sorted close times in CSV format
def getListCloseOnlyTopCSV(k):

    list_close = io.StringIO()
    new_str = ""

    dict = []

    # Since limit(0) returns all, this fixes that and returns nothing
    if k != 0:
        for i in db.collection.find().sort([('control_open', 1)]).limit(k):
            dict.append({
                'control' : i['control'],
                'distance' : i['distance'],
                'control_close' : i['control_close'],
            })

        writer = csv.writer(list_close, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(dict)

        # Tweaking the formatting of the CSV string a bit...
        new_str = formatCSVString(str(list_close.getvalue()))

    return new_str

# formatCSVString(str) - helper function that formats
# a string to something closer to CSV format
def formatCSVString(str):
   temp1 = str.replace('\"', '')
   temp2 = temp1.replace(',{', ', {')
   new_str = temp2.replace('\r\n', '')
   return new_str

## *** Classes for each path ***

class ListAll(Resource):
    @api_token_required
    def get(self):
        return getListAllJSON()

class ListAllParam(Resource):
    @api_token_required
    def get(self, param):
        if param == "csv":
            return getListAllCSV()
        elif param == "json":
            return getListAllJSON()
        
        return "Invalid path (listAll)"

class ListOpenOnly(Resource):
    @api_token_required
    def get(self):
        return getListOpenOnlyJSON()

class ListOpenOnlyParam(Resource):
    @api_token_required
    def get(self, param):
        k = request.args.get('top', type=int)

        # Makes sure we're passing an int
        if isinstance(k, int):
            if param == "csv":
                return getListOpenOnlyTopCSV(k)
            elif param == "json":
                return getListOpenOnlyTopJSON(k)

        # If we're not dealing with top entries,
        # we just return something in csv or JSON
        if param == "csv":
            return getListOpenOnlyCSV()
        elif param == "json":
            return getListOpenOnlyJSON()

        return "Invalid path (listOpenOnly)"

class ListCloseOnly(Resource):
    @api_token_required
    def get(self):
        return getListCloseOnlyJSON()

class ListCloseOnlyParam(Resource):
    @api_token_required
    def get(self, param):
        k = request.args.get('top', type=int)

        # Makes sure we're passing an int
        if isinstance(k, int):
            if param == "csv":
                return getListCloseOnlyTopCSV(k)
            elif param == "json":
                return getListCloseOnlyTopJSON(k)

        # If we're not dealing with top entries,
        # we just return something in csv or JSON
        if param == "csv":
            return getListCloseOnlyCSV()
        elif param == "json":
            return getListCloseOnlyJSON()

        return "Invalid path (listCloseOnly)"

# Creates routes
api.add_resource(ListAll, '/listAll', '/listAll/')
api.add_resource(ListAllParam, '/listAll/<string:param>')
api.add_resource(ListOpenOnly, '/listOpenOnly', '/listOpenOnly/')
api.add_resource(ListOpenOnlyParam, '/listOpenOnly/<string:param>')
api.add_resource(ListCloseOnly, '/listCloseOnly', '/listCloseOnly/')
api.add_resource(ListCloseOnlyParam, '/listCloseOnly/<string:param>')
api.add_resource(Register, '/api/register', '/api/register/')
api.add_resource(Token, '/api/token', '/api/token/')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
